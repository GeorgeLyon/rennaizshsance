# Rennaizshsance

A collection of `zsh` extensions used by me

## Usage (with [`oh-my-zsh`](https://ohmyz.sh))

The following will enable all the plugins in this repo. Please note that in `oh-my-zsh` you can have only one `ZSH_CUSTOM` directory, so an alternative approach would be to copy the plugins over to your own `ZSH_CUSTOM` directory. Also, there are dpendencies between the plugins, so they cannot be used a-la-carte.

```
ZSH_CUSTOM=<Path-to-this-repo>
precmd_functions=()
preexec_functions=()
chpwd_functions=()
plugins=(
  strings
  george_paths
  cd_recent
  select_one 
  caffeinate
)
source $ZSH/oh-my-zsh.sh
```

## Plugins

### Caffeinate

Runs `caffeinate -s` while any command is running in your shell. This prevents your computer from going to sleep when connected to power for the duration of your commands.

### `cd_recent`

Once installed, this plugin maintains a history of `zsh` working directories. The `cd_recent` command displays a set of recent working directories using `select_one` and navigates to the selected location. I run this command as the last line in my `.zshrc` so every new terminal window prompts me to select a recent working directory. 

