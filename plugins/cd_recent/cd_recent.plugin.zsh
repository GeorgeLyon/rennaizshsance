local pwd_history_file=$ZDOTDIR/.pwd_history

trim_invalid_paths() {
  local path
  while read path; do
    if [[ -d "$path" ]]; then
      echo $path
    fi
  done
}

cd_recent_chpwd() {
  local entries=()
  cat "$pwd_history_file" | while read dir; do 
    entries+=$dir; 
  done
  entries+=$(pwd -P)
  for entry in $entries; do echo $entry; done |
    tail -r |
    awk '!x[$0]++' |
    tail -r |
    tail -n 100 > $pwd_history_file
}
chpwd_functions+=cd_recent_chpwd

recent_paths() {
  cat "$pwd_history_file" |
    grep -ve "^$(pwd -P)\$" |
    trim_invalid_paths
}

cdrecent() {
  selected=$(
    recent_paths |
    (cat; if [[ "$1" == "--include-pwd" ]]; then echo $(pwd -P); fi) |
    tail -rn 10 |
    select-one -f cleanup-path)
  builtin cd "$selected"
}

cd() {
  if (($# == 0)); then
    cdrecent
  else
    builtin cd $@
  fi
}

