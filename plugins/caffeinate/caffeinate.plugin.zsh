caffeinate_preexec() {
  ( caffeinate -s &; echo $! ) | read CAFFEINATE_PID
}
caffeinate_precmd() {
  if [ ! -z "$CAFFEINATE_PID" ]; then
    kill $CAFFEINATE_PID
    CAFFEINATE_PID=""
  fi
}
preexec_functions+=caffeinate_preexec
precmd_functions+=caffeinate_precmd
