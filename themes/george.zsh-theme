#!/bin/zsh

prompt_precmd() {
  if ! xcode-select -p 2> /dev/null >/dev/null; then
    print -n "🚨 Xcode is not installed 🚨 >"
    return 0
  fi

  local print_bold() { print -n "%B${@}%b" }
  local print_color() { local color=$1; shift; print -n "%F{$color}${@}%f" }

  local print_hostname() { print_color "green" $@ }
  local print_path() { print_bold $(print_color "yellow" $@) }
  local print_git() { print_bold $(print_color "magenta" $@) }
  local print_git_auxiliary() { print_bold $(print_color "240" $@) }
  local print_git_detached() { print_bold $(print_color "red" $@) }

  local prompt_lines=()

  # If we are in SSH, add the hostname
  if [ -n "$SSH_CLIENT" ]; then
    local hostname=$(scutil --get LocalHostName | tr '-' ' ' | sed "s/Georges/George's/")
    prompt_lines+=$(print_hostname $hostname)
  fi

  # Alert if CarbonBlack is running
  if [ -n "$(kextstat -l -b com.carbonblack.CbOsxSensorProcmon)" ]; then
    prompt_lines+="🚨 CarbonBlack is running 🚨"
  fi

  # Add working directory path with git info
  prompt_lines+=$(function {
    # Get git info
    local GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
    local GIT_SHORT_SHA=$(git rev-parse --short HEAD 2>/dev/null)
    local GIT_ROOT=$(git rev-parse --show-toplevel 2>/dev/null)
    local GIT_PATH=${"$(pwd -P)"##"$GIT_ROOT"}
    local GIT_BRANCH_PREFIX=""
    for separator in "--" "/"; do
    if [[ "${GIT_BRANCH}" =~ "^([^-]+${separator})(.*)\$" ]]; then
      local GIT_BRANCH_PREFIX=$match[1]
      local GIT_BRANCH=$match[2]
      break
    fi
    done
    local GIT_BRANCH_SUFFIX=""
    if [[ "${GIT_BRANCH}" =~ '^(.*)(-[0-9]+)$' ]]; then
      local GIT_BRANCH=$match[1]
      local GIT_BRANCH_SUFFIX=$match[2]
    elif [[ "${GIT_BRANCH}" =~ '^(.*)(--.*)$' ]]; then
      local GIT_BRANCH=$match[1]
      local GIT_BRANCH_SUFFIX=$match[2]
    fi

    # Print path with git info
    if [[ -z "$GIT_ROOT" ]]; then
      # Not a git repo
      print_path "$(pwd | cleanup-path)"
    else
      if [[ "$(basename $GIT_ROOT)" =~ "($GIT_BRANCH)($GIT_BRANCH_SUFFIX)?" ]]; then
        print_path "$(dirname $GIT_ROOT | cleanup-path)/"
        # print_git_auxiliary "$GIT_BRANCH_PREFIX"
        print_git "${GIT_BRANCH//-/%F\{240\}-%F\{magenta\}}"
        print_git_auxiliary "$GIT_BRANCH_SUFFIX"
      elif [[ "$GIT_BRANCH" == "HEAD" ]]; then
        print_path "$(echo $GIT_ROOT | cleanup-path)"
        print_git_auxiliary " at "
        print_git_detached "$GIT_SHORT_SHA"
      else
        print_path $(echo $GIT_ROOT | cleanup-path)
        print_git_auxiliary " on "
        print_git "${GIT_BRANCH_PREFIX}${GIT_BRANCH}${GIT_BRANCH_SUFFIX}"
      fi

      if [[ ! -z "$GIT_PATH" ]]; then
        print_path "$GIT_PATH"
      fi
    fi
  })

  local print_prompt_line() { local box_character=$1; shift; print -P "\r%B${box_character}%-5<╌╌ < ${@}" }
  print_prompt_line "╭" ${prompt_lines[1]}
  for line in ${prompt_lines[2,-1]}; do
    print_prompt_line "├" $line
  done
}
precmd_functions+=prompt_precmd

PROMPT="%r%B╰─○%b "
